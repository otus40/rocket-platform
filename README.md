# Rocket Platform



Проект *Rocket Platform* представляет собой MVP инфраструктурной платформы для запуска веб-приложения, построенного по принципу микросервисной архитектуры.

Проект разрабатывается с целью закрепления и демонстрации полученных знаний и навыков на курсе **Инфраструктурная платформа на основе Kubernetes** (https://otus.ru/lessons/infrastrukturnaya-platforma-na-osnove-kubernetes/).



## Концепция

Платформа *Rocket Platform* реализует традиционный цикл разработки и запуска микросервисного приложения [Online Boutique](https://github.com/GoogleCloudPlatform/microservices-demo) на базе Kubernetes. 

Особенности:

* инфраструктурные компоненты устанавливаются с использованием `helm` , микросервисное приложение - с использование `flux`;
* для приложения создаются отдельные пространства имен - тестовое, рабочее;
* при изменении исходного кода компонентов приложение автоматически выполняется сборка артефактов - docker-образов и helm-схем; 
* при изменениях в хранилище артефактов автоматически выполняется развертывание в k8s; docker-образы из git-ветки по умолчанию попадают в тестовое пространство имен, *тегирование* запускает приложение в рабочую среду;
* Gitlab-раннеры запускаются непосредственно на платформе (для экономии времени загрузки и платных минут CI на gitlab.com)
* развертывание приложений методом RollingUpdate;
* доступ к веб-приложениям через https;
* логирование с использованием loki/promtail, метирки - prometheus, дашборды - графана!



## Структура каталогов группы проектов

https://gitlab.com/otus40 - группа проектов платформы

https://gitlab.com/otus40/rocket-platform - описание и параметры установки платформы

https://gitlab.com/otus40/boutique - группа проектов с исходным кодом  [Online Boutique](https://github.com/GoogleCloudPlatform/microservices-demo)

https://gitlab.com/otus40/helmcharts - группа проектов с helm-чартами для Boutique

https://gitlab.com/otus40/ci - проект с конфигурацией CI для всех проектов



## Артефакты

https://rocket-platform.ru - веб-приложение (*production*)

https://test.rocket-platform.ru - веб-приложение (*test*)

https://grafana.rocket-platform.ru - метрики и логи

https://harbor.rocket-platform.ru - хранилище docker-образов

https://minio.rocket-platform.ru - S3-хранилище общего назначения. Планируется использовать для кэша раннеров gitlab.



## Перспективы развития (TODO)

* использование flux для установки инфраструктуры;
* хранение секретов Hashicorp Vault;
* *канареечное* развертывание приложения с использованием Istio / Flagger;
* дашборды компонентов приложения, алерты;
* установка всех компонентов магазина.



## Online Boutique

После подготовки инфраструктуры (см. раздел "Инфраструктура") выполняется установка приложения с использованием Kustomize.

В директории */flux/apps/base* создаются базовые файлы ресурсов, такие как helm-релизы схем компонентов приложения, ресурсы репозиториев артефактов и политик обновления.

Для тестового и рабочего окружения создаются соответствующие директории */flux/apps/boutique-test* и */flux/apps/boutique* с ресурсами, имеющими специфические параметры.  К ним относятся, среди прочего, метки докер образов helm-релизов и конфигурация обновления компонентов.

Для каждого пространства имен в директории */clusters/rocket* создаем директорию с соответствующим наименованием и добавляем *Kustomization* ресурс со ссылкой на соответствующую директорию в */flux/apps*.

После завершения изменений и выполнении `git push` выполняется автоматическое создание пространств имен и запускается два экземпляра приложения. 

При внесении изменений в helm-схемы приложения, перезапуск соответствующего компонента выполняется автоматически.

При изменении исходного кода приложения автоматически корректируется метка образа в данном проекте и выполняется обновление приложения.



## Инфраструктура

### Kubernetes

Платформа запускается *поверх* **Yandex Managed Service for Kubernetes**.

Версия кластера 1.23

Характеристики рабочих узлов:

* количество узлов - 4
* vCPU - 2
* RAM - 6 ГБ
* HDD - 64 ГБ
* Прерываемая



### Ingress Nginx

Refs:

* https://kubernetes.github.io/ingress-nginx/deploy/#quick-start
* https://github.com/kubernetes/ingress-nginx/tree/main/charts/ingress-nginx

```bash
helm upgrade --install ingress-nginx ingress-nginx \
  --repo https://kubernetes.github.io/ingress-nginx \
  --namespace ingress-nginx \
  --create-namespace \
  --values ingress-nginx/ingress-nginx-values.yaml \
  --wait
```



### Cert Manager

Refs:

* https://cert-manager.io/docs/tutorials/acme/nginx-ingress/,
* https://cert-manager.io/docs/installation/helm/#steps
* https://artifacthub.io/packages/helm/cert-manager/cert-manager

```bash
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm upgrade --install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --create-namespace \
  --version v1.11.0 \
  --set installCRDs=true \
  --values cert-manager/cert-manager-values.yaml \
  --wait
# --- create certificate issuer
kubectl apply -f cert-manager/letsencrypt-prod-issuer.yaml --namespace cert-manager
```



### Loki

Refs:

* https://grafana.com/docs/loki/latest/installation/helm/install-scalable/
* https://github.com/grafana/helm-charts/tree/main/charts/promtail

```bash
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
# --- loki
helm upgrade --install loki grafana/loki \
  --namespace loki \
  --create-namespace \
  --values loki/loki-values.yaml \
  --wait
# --- promtail
helm upgrade --install promtail grafana/promtail \
  --namespace loki \
  --values loki/promtail-values.yaml \
  --wait
```



### Prometheus

Ref. https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack

```bash
kubectl create namespace prometheus
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm upgrade --install prom prometheus-community/kube-prometheus-stack \
  --namespace prometheus \
  --create-namespace \
  --values prometheus/prometheus-values.yaml \
  --wait
```

Prometheus разворачивается с большим количеством панелей для kubernetes, тем не менее следующие панели будут также полезны:

* ingress-nginx - https://grafana.com/grafana/dashboards/9614-nginx-ingress-controller/
* cert-manager - TODO: find or create
* loki/promtail - https://grafana.com/grafana/dashboards/14055-loki-stack-monitoring-promtail-loki/
* chartmuseum - https://grafana.com/grafana/dashboards/9622-chartmuseum/
* harbor - https://grafana.com/grafana/dashboards/16003-harbor/ (или https://grafana.com/grafana/dashboards/14075-harbor/)



### Chartmuseum

> Для хранения helm-схем используется Harbor. Раздел сохранен здесь для примера запуска.

Refs:

* https://chartmuseum.com/
* https://artifacthub.io/packages/helm/chartmuseum/chartmuseum

```bash
helm repo add chartmuseum https://chartmuseum.github.io/charts
helm repo update
helm upgrade --install chartmuseum chartmuseum/chartmuseum \
  --namespace chartmuseum \
  --create-namespace \
  --values chartmuseum/chartmuseum-values.yaml \
  --version 3.9.3 \
  --wait
```

FIXME: Метрики отдаются в открытом виде!



### Harbor

Refs:

* https://goharbor.io/docs/2.7.0/install-config/
* https://artifacthub.io/packages/helm/harbor/harbor

```bash
helm repo add harbor https://helm.goharbor.io
helm repo update
helm upgrade --install harbor harbor/harbor \
  --namespace harbor \
  --create-namespace \
  --values harbor/harbor-values.yaml \
  --version 1.11.0 \
  --wait
```



### Minio Bitnami

Refs:

* https://artifacthub.io/packages/helm/bitnami/minio

```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
helm upgrade --install minio bitnami/minio \
  --namespace minio \
  --create-namespace \
  --values minio/minio-values.yaml \
  --version 12.1.1 \
  --wait
```

Изменение пароля выполняется через secret-ресурса *minio*

```yaml
# ...
stringData:
  root-user: user
  root-password: secret
```

ВНИМАНИЕ. Веб-интерфейс minio через port-forwarding не работает.



### Gitlab Runners

Refs:

* https://docs.gitlab.com/runner/install/kubernetes.html
* https://artifacthub.io/packages/helm/gitlab/gitlab-runner

```bash
helm repo add gitlab http://charts.gitlab.io/
helm repo update
helm upgrade --install gitlab-runner gitlab/gitlab-runner \
  --namespace gitlab \
  --values gitlab/gitlab-runner-values.yaml \
  --set runnerRegistrationToken="use-group-registration-token" \
  --version 0.47.0 \
  --wait

# --- create admin binding (need for helm dry install)
kubeclt apply -f gitlab/default-rolebinding.yaml -n gitlab
```



### Flux

Refs:

* https://fluxcd.io/flux/installation/
* https://fluxcd.io/flux/installation/#gitlab-and-gitlab-enterprise

```bash
export GITLAB_TOKEN=secret
flux bootstrap gitlab \
  --components-extra=image-reflector-controller,image-automation-controller \
  --owner=otus40 \
  --repository=rocket-platform \
  --branch=main \
  --path=clusters/rocket \
  --token-auth \
  --personal
git pull
```

Далее необходимо создать ресурсы `Secret` с параметрами доступа к  helm и docker репозиториям

```bash
# --- helm repo
kubectl create secret generic rocket-creds \
  -n boutique \
  --from-literal=username=user --from-literal=password=secret
# --- docker repo
kubectl create secret docker-registry rocket-docker-creds \
  -n boutique \
  --docker-server=harbor.rocket-platform.ru \
  --docker-username=user \
  --docker-password=secret \
  --docker-email=foo@example.com
```

 В директории */clusters/rocket* можно создавать директории пространств имен с ресурсами *Namespace, HelmRepository, HelmRelease*  и т.п. для развертывания приложений.



